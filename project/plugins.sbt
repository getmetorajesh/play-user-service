addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.13")
addSbtPlugin("com.iheart" %% "sbt-play-swagger" % "0.7.4")
addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.0.1")
