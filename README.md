# User Service Boilerplate
More often than not I keep coming across a common task of building a `UserService` with usual endpoints for registering, login, logout with JWT tokens and setting up common libraries(Coursier, JWT, swagger).

This is an attempt to reduce the repetitive task by having a common biolerplate that I can reuse across my projects. 

# Swagger
- Base swagger is in conf/swagger.yml
- playSwagger will automatically fillin the build version `info.version`. Can also override in swagger.yml file.
- Update domainPackage in ApiSpecs.scala. This controller is used to serve swagger ui


# WIP:
- SwaggerUI is blocked due to `Play 2.6` security restrictions. Keep an eye on https://github.com/iheartradio/play-swagger/issues/174

