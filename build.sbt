name := """play-user-service"""
organization := "com.example"

version := "1.0-SNAPSHOT"

resolvers += Resolver.jcenterRepo
// add swaggerPlugin
lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

// add domain package names for play-swagger to auto generate swagger definitions for domain classes mentioned in your routes
swaggerDomainNameSpaces := Seq("models")

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.webjars" % "swagger-ui" % "2.2.0",
  "com.iheart" %% "play-swagger" % "0.7.4")

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
