package controllers.swagger

import play.api.Configuration
import com.iheart.playSwagger.SwaggerSpecGenerator
import play.api.libs.json.JsString
import play.api.mvc._
import javax.inject._


@Singleton
class ApiSpecs@Inject()(cc: ControllerComponents, config: Configuration) extends AbstractController(cc) {
  implicit val cl = getClass.getClassLoader

  val domainPackage = false; //Seq("YOUR.DOMAIN.PACKAGE")
  val otherDomainPackage = "YOUR.OtherDOMAIN.PACKAGE"
  lazy val generator = SwaggerSpecGenerator(domainPackage, otherDomainPackage)

  // Get's host configuration.
  val host = "localhost:9000";// config.get[String]("swagger.host")

  lazy val swagger = Action { request =>
    generator.generate().map(_ + ("host" -> JsString(host))).fold(
      e => InternalServerError("Couldn't generate swagger."),
      s => Ok(s))
  }

  def specs = swagger
}