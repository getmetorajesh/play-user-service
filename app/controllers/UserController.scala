package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class UserController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def get() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def post() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def login() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def register() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def getBy(id: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def verifyEmail(token: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def resetPassword(id: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def newPassword(id: String, old_password: String, new_password: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
}

